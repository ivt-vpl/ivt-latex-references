Fellendorf, M. and P. Vortisch (2000) Integrated modelling of transport demand, route choice and traffic, paper presented at the 79th Annual Meeting of the Transportation Research Board, Washington D.C., January 2000.
Fellendorf, M., T. Haupt, U. Heidl and W. Scherr (1997) PTV Vision: Activity-based microsimulation model for travel demand forecasting, in D.F. Ettema and H.J.P. Timmermans (eds.) Activity-based approaches to travel analysis, 55-72, Pergamon, Oxford.
Hartmann, M., N. Motamedidehkordi, S. Hoffmann, P. Vortisch, F. Busch (2017) Impact of Automated Vehicles on Capacity of the German Freeway Network, Vortrag, ITS World Congress 2017, Montreal, October.

Loder, A., L. Ambühl, M. Menendez and K.W. Axhausen (2017) Empirics of multi-modal traffic networks – Using the 3D macroscopic fundamental diagram, Transportation Research Part C, 82, 88-101.
Loder, A., L. Ambühl, M. Menendez and K.W. Axhausen. 2017. Traffic problems in towns - An empirical analysis with macroscopic fundamental diagrams from cities around the world, Vortrag, 97th Annual Meeting of the Transportation Research Board, Washington, D.C., Januar.
Loewenstein, G. and D. Prelec (1992). Anomalies in Intertemporal Choice: Evidence and an Interpretation, The Quarterly Journal of Economics, 107, 573-97.
Löffler, M. (2015) Measuring willingness to pay: Do direct methods work for premium durables?, Marketting Letters, 26 (4) 535-548. 

Timmermans, H.J.P., A. Borgers, J. van Dijk and H. Oppewal (1992) Residential choice behaviour of dual earner households: a decompositional joint choice model, Environment and Planning A, 24 (3) 517-533.

Park J. Y., Kim D. J., and Lim Y. T. (2008). Use of Smart Card to Define Public Transit Use in Seoul, South Korea. In Transportation Research Record: Journal of the Transportation Research Board, No. 2063, Transportation Research Board of the National Academies, Washington, D.C., pp. 3–9.
