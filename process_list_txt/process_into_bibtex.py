import re
import pprint
import operator
import json
import itertools
from operator import itemgetter

from parsing_functions import *


with open("axhausen_lit_list.txt") as myfile:
#with open("process_list_txt/tricky_test_input.txt") as myfile:
    head = [s for s in myfile.readlines() if len(s) > 5]

    processed_records = []
    for ref in head:
        print(ref[:80])
        ref_details = split_on_year(ref)
        
        if ref_details: 
            #make author string
            ref_details['author'] = make_author_list(split_authors(ref_details['author']))
            print (ref_details['author'] )
            #process the rest
            ref_details = detect_end_of_title(ref_details)
            if ref_details:
                ref_details = process_journal_details(ref_details)
                processed_records.append(ref_details)

                #take first real word
    #            print(ref_details['title'])
                author_key = re.split(r'\s|,', ref_details['author'])[0]

                title_first_word = [w for w in re.findall(r'\S+', ref_details['title'], re.I) if len(w) > 3][0]
                ref_details['ID'] = author_key + ref_details['year'] + title_first_word.lower()
                ref_details['ID'] = re.sub('\W', '', ref_details['ID']) # remove all no word elements
    #            print(ref_details)
    #            print ('---------------------------')

    #remove null rests:
    processed_records = [{ k: v for k, v in d.items() if v } for d in processed_records]
    #move rest to notes
    for d in processed_records:
        if 'rest' in d:
            d['note'] = d.pop('rest')
        if 'pages' in d:
            d['pages'] = re.sub(r'(?:-|–)+', '--', d['pages'])
        

    processed_records = sorted(processed_records, key=operator.itemgetter('author', 'year'))
    print(processed_records[:4])
    processed_records_dict = dict(
        (k, sorted(list(g), key=lambda x: -len(x))[0])
        for k, g in itertools.groupby(processed_records, key=itemgetter('ID')))


    with open("axhausen_lit_list_processed.json", 'w', encoding='utf8') as out_json:
        json.dump(list(processed_records_dict.values()), out_json, indent=2, ensure_ascii=False)

    print(list(processed_records_dict.values())[:4])

    from bibtexparser.bwriter import BibTexWriter
    from bibtexparser.bibdatabase import BibDatabase

    db = BibDatabase()
    db.entries = list(processed_records_dict.values())

    writer = BibTexWriter()
    with open('ivt_references.bib', 'w') as bibfile:
        bibfile.write(writer.write(db))

    num_records = len(head)
    procssed_records = len(processed_records)
    unique_records_title = len({r['title'] for r in processed_records})
    unique_records_id = len({r['ID'] for r in processed_records})
    full_records = len({r['title'] for r in processed_records if 'note' not in r})
    print('Number of original references: ', num_records)
    print('Records successfully processed:', procssed_records)
    print('Unique records (title):         ', unique_records_title)
    print('Unique records (id):            ', unique_records_id)
    print('Fully complete references:     ', full_records)
    #big question is if the title has a comma in it?

    #after is the title, until the next word based 

    #until next comma is the journal or paper
    #    - paper presented at ....

    #until end is the edition/volume/pages - 74, 333-340/ 24 (1) 7‑21

#print('\n'.join(head))