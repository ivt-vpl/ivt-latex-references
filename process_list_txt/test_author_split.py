
from parsing_functions import *

with open("process_list_txt/author_test_input.txt") as myfile:
    head = [s for s in myfile.readlines() if len(s) > 5]

    processed_records = []
    for ref in head:
        ref_details = split_on_year(ref)
        print(ref_details)
        if ref_details:
            author_split = split_authors(ref_details['author'])
            print(author_split)
            print()
        