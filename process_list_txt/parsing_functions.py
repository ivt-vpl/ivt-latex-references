import re
import pprint
import operator
import json
import itertools
from operator import itemgetter

def split_authors(s):
    s = re.sub(',? and ', ', ', s)
    s = re.sub(',? und ', ', ', s)
    s = re.sub(',? &', ', ', s)
    s = s.replace(' et al.', '')

    num_commas = s.count(',')
    a_list = [s1.strip() for s1 in s.rsplit(',', num_commas-1)]

    return a_list

def make_author_list(ss):
    return ' and '.join(ss)


def split_on_year(s):
    #get location of year
    re1 = re.search('\(\d{4}\)', s)
    if re1 is None:
        bad_references.append(dict(ref=s, reason='no year'))
        return None
    
    year = re1.group()[1:-1]
    year_start, year_end = re1.span()
    #before is the author
    authors = s[:year_start].strip()
    authors = re.sub(r'\bund\b', 'and', authors) #replace und with and
    authors = re.sub(r'\b&\b', 'and', authors) #replace & with and

    if authors.count(',') > 1 and ' and ' not in authors: #if no and, replace last comma with and
        authors = ' and '.join([a.strip() for a in authors.rsplit(',', 1)])

    rest = s[year_end:]

    return dict(author=authors, year=year, rest=rest, ENTRYTYPE='misc')

def detect_end_of_title(ref_details):
    #get location of year
    s = ref_details['rest']
    if '.' == s[0]: #remove fullstop at start if there
        s = s[1:].strip()

    re1 = re.search(journal_regex, s, re.I)
    if re1:
        ref_details['title'] = s[:re1.span()[0]].strip()
        ref_details['rest'] = s[re1.span()[0]+2:].strip()
    else:
        ss = re.split(r'(?<=[^(?:,|\.)]{8})(?:,|\.)', s, 1)
        if len(ss) > 1:
            ref_details['title'] = ss[0].strip()
            ref_details['rest'] = ss[1].strip()
        else:
            bad_references.append(dict(ref=s, reason='bad title'))
            ref_details = None
    return ref_details



journal_starts = [
    'Transportation Research',
    'Transportation Research Part',
    'Journal of',
    'Schlussbericht',
    'Schriftenreihe',
    'VSS',
    'paper presented',
    'Doctoral Thesis'
    'Master Thesis',
    'IEEE',
    'in International Association',
    'Transportation Research Record',
    'Management Science',
    'Computer-Aided Civil and Infrastructure Engineering',
    'New Society'

]

bad_references = []

journal_regex = '|'.join(['(?:,|.)\s+\\b'+s+'\\b' for s in journal_starts])
print (journal_regex)
print("\n")

possible_fields = ['journal', 'volume', 'pages', 'issue', 'institution', 'number', 'ENTRYTYPE', 'address', 'month', 'school', ]

journal_regexes = [
   # ('booklet', r'(?P<howpublished>(?:paper )?presented at.*?),\s+(?P<address>.*?(?:,? D.C.)?),\s+(?P<month>.*?)\s+'), #conference paper
    ('booklet', r'(?P<howpublished>(?:paper )?presented at.*)'), #conference paper
   
    ('techreport', r'Schriftenreihe,\s?(?P<number>\d+), (?P<institution>.*)'), #other schriftreihe
    ('incollection', r'in (?P<editor>.*(?=\()+)\(\s*[eE]ds?.\)\s+(?P<booktitle>.*?)\s*,? (?P<pages>\d+(?:-|–)+\d+),\s+(?P<publisher>.*(?=,)*?),\s*(?P<address>[a-zA-ZäöüÄÖÜß ]*)'),
    ('article', r'(?P<journal>.*?),\s?(?P<volume>\d+),\s*(?P<pages>(?:\d|-|–)+)?'), # journal no issue
    ('article', r'(?P<journal>.*?),\s?(?P<volume>\d+) \(x?\) \s*(?P<pages>(?:\d|-|–)+)?'), #journal with x issue
    ('article', r'(?P<journal>.*?),?\s?(?P<volume>\d+)\s*\((?P<issue>(?:\d|-|–)+)\)\s*(?P<pages>(?:\d|-|–)+)?'), #journal  with full rec
    ('article', r'(?P<journal>.*?)(?=\d)(?P<volume>\d+):(?P<issue>\d+)(?:,|\s)+(?P<pages>(?:\d|-|–)+)?'), #journal with : notation
    ('article', r'(?P<journal>.*?)\:?,?\s?(?P<volume>\d+)[\:,]?\s*(?P<pages>(?:\d|-|–)+)'), #weird TRR format
    
    ('article', r'(?P<journal>.*?)(?:,|\s)+(?P<volume>\d+) \((?P<issue>(?:\d|-|–)+)\)\s*(?P<pages>(?:\d|-|–)+)?'), #full journal rec and space
    ('book', r'^(?P<publisher>[^,]*?),\s*(?P<address>[a-zA-ZäöüÄÖÜß ]*)\W*$'), #book
    ('book', r'^(?P<edition>\d+th edition)[,\.]?\s?(?P<publisher>[^,]*?),\s*(?P<address>[a-zA-ZäöüÄÖÜß ]*)\W*$'), #book
    ('techreport', r'^Schriftenreihe,\s?(?P<publisher>.*(?=,)*?),\s*(?P<address>[a-zA-ZäöüÄÖÜß ]*)\W*$'), #schriftreihe
    ('phdthesis', r'^Dissertation,\s?(?P<school>.*(?=,)*?),\s*(?P<address>[a-zA-ZäöüÄÖÜß ]*)\W*$'), #phdtheisis
    ('phdthesis', r'PhD thesis, (?P<school>[^,]*?), (?P<address>[^.]*)') #phdthesis
]

def process_journal_details(ref_details):
    re1 = None

    for (bibtex_ENTRYTYPE, j_regex) in iter(journal_regexes):
        re1 = re.match(j_regex, ref_details['rest'], re.UNICODE)
        if re1:
            break

    if re1:
        ref_details['rest'] = None
        ref_details['ENTRYTYPE'] = bibtex_ENTRYTYPE
        trimmed_groups = { k:v.strip() for k, v in re1.groupdict().items() if v }
        ref_details.update(trimmed_groups)

    return ref_details