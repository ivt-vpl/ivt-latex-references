This is a bibtex collection of references collected at IVT over the past years. 

It was parsed from a text based list of references using the python script in the repository, which automatically converted around half of the ~8000 items, while trying to detect and remove duplicates.

Left over parts of incomplete references can be found in the notes field of the respective reference, which will need to be incorportated manually into the bibtex item, or by improving the supplied script. 

There is no guarantee that the bibtex references are complete or accurate. Many are missing information other than the authors, year and title.

## Condition of using this file:
Any new items, updates or corrected references must also be updated in the bibtex file in this repository.

## Bibtex key convention
For this bibtex, roughly the same naming convention is used as found in google scholar refernces
- [last name of first author][year][first significant word of title]
- i.e. 
`
@article{Axhausen1986Bicyclist,
 author = {Axhausen, K.W. and R.L. Smith},
 journal = {Transportation Research Record},
 pages = {7--15},
 title = {Bicyclist Link Evaluation: A Stated-Preference Approach},
 volume = {1085},
 year = {1986}
}
`